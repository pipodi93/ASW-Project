export class PlaceToSave {

  day: string;
  placeID: string;

  constructor (day: string, placeID: string){
    this.day = day;
    this.placeID = placeID;
  }
}
