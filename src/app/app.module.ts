import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AppRoutingModule} from "./app-routing/app-routing.module";

import {FillHeightModule} from "ngx-fill-height/fill-height.module";

import {AppComponent} from "./app.component";
import {HomeComponent} from "./home/home.component";
import {IndexComponent} from "./index/index.component";
import {GroupsComponent} from "./groups/groups.component";
import {ProfileComponent} from "./profile/profile.component";
import {MyDateRangePickerModule} from "mydaterangepicker";
import {NguiAutoCompleteModule} from "@ngui/auto-complete";
import {SuggestComponent} from "./suggest/suggest.component";


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    IndexComponent,
    GroupsComponent,
    ProfileComponent,
    SuggestComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    MyDateRangePickerModule,
    FillHeightModule,
    NguiAutoCompleteModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
