import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import "rxjs/add/operator/toPromise";
import {City} from "../model/city";

@Injectable()
export class IndexService {

  private urlRegister: string = 'http://ubook.store/svapo/registerPost.php';
  private urlLogin: string = 'http://ubook.store/svapo/login.php';
  private urlCities: string = 'http://ubook.store/svapo/cities.php';

  constructor(private http: Http) { }

  exampleGet(email:string, password:string, name:string, surname:string): Promise<string>{
    var params = 'email=' + email + '&' + 'password=' + password + '&' + 'name=' + name + '&' + 'surname=' + surname;

    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    return this.http.get(this.urlRegister + '?' + params, headers)
      .toPromise().then(result => result.json());
  }

  register(email:string, password:string, name:string, surname:string): Promise<string>{
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {email: email, password: password, name: name, surname:surname};

    return this.http.post(this.urlRegister, JSON.stringify(params), headers)
      .toPromise().then(result => result.json());
  }

  login(email:string, password:string): Promise<string>{
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {email: email, password: password};

    return this.http.post(this.urlLogin, JSON.stringify(params), headers)
      .toPromise().then(result => result.json());
  }

  retrieveCities(): Promise<City[]> {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {};

    return this.http.post(this.urlCities, JSON.stringify(params), headers)
      .toPromise().then(result => result.json() as City[]);
  }

}
