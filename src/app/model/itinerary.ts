import {Route} from "./route";
import {PlaceItinerary} from "./place-itinerary";

export class Itinerary {

  id: string;
  startDay: string;
  endDay: string;
  userName: string;
  surname: string;
  places: PlaceItinerary[];
  cityName: string;

  constructor(id: string, startDay: string, endDay: string, userName: string, surname: string, cityName: string){
    this.id = id;
    this.startDay = startDay;
    this.endDay = endDay;
    this.userName = userName;
    this.surname = surname;
    this.cityName = cityName;
  }

  setPlaces(places: PlaceItinerary[]): void {
    this.places = places;
  }

}
