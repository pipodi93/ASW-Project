import {Place} from "./place";

export class Route {

  day: string;
  dayString: string;
  places: Place[];

  constructor(day: string, dayString: string, places: Place[]){
    this.day = day;
    this.dayString = dayString;
    this.places = places;
  }

}
