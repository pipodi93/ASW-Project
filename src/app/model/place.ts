export class Place {

  placeID: string;
  placeName: string;
  placeDescription: string;
  image: string;
  lat: number;
  lng: number;
  duration: number;
  rate: number;
  distanceTo: number;
  distanceInMinutes: number;

  address: string;
  longDescription: string;
  link: string;
  linkType: string;

}
