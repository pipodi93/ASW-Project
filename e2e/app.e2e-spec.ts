import { RoadToSvapoPage } from './app.po';

describe('road-to-svapo App', () => {
  let page: RoadToSvapoPage;

  beforeEach(() => {
    page = new RoadToSvapoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
