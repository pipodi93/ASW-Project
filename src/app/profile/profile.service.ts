import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {City} from "../model/city";

@Injectable()
export class ProfileService {

  private urlItineraries: string = 'http://ubook.store/svapo/ownItineraries.php';
  private urlCities: string = 'http://ubook.store/svapo/cities.php';

  constructor(private http: Http) {
  }

  retrieveItineraries(email: string): Promise<string> {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {email: email};

    return this.http.post(this.urlItineraries, JSON.stringify(params), headers).toPromise()
      .then(result => result.json());
  }

  retrieveCities(): Promise<City[]> {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {};

    return this.http.post(this.urlCities, JSON.stringify(params), headers)
      .toPromise().then(result => result.json() as City[]);
  }
}
