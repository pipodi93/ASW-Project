import {Component, OnInit} from "@angular/core";
import {IMyDateRangeModel, IMyDrpOptions, IMyInputFieldChanged} from "mydaterangepicker";
import {User} from "../model/user";
import {Router} from "@angular/router";
import {ProfileService} from "./profile.service";
import {PlaceItinerary} from "../model/place-itinerary";
import {Itinerary} from "../model/itinerary";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [ProfileService]
})
export class ProfileComponent implements OnInit {

  private user: User;
  private type: string;
  private itineraries: Itinerary[] = [];

  private currentDate: Date;
  private checkInDateMillis: number = null;
  private checkOutDateMillis: number = null;
  private cities: string[];
  private selectedCity: string = '';

  private myDateRangePickerOptions: IMyDrpOptions;

  constructor(private router: Router, private service: ProfileService) {
  }

  ngOnInit() {
    if (localStorage.getItem('email') != null) {
      this.user = new User(localStorage.getItem('email'), localStorage.getItem('name'), localStorage.getItem('surname'));
    } else {
      this.user = null;
    }
    this.currentDate = new Date();
    this.myDateRangePickerOptions = {
      dateFormat: 'dd/mm/yyyy',
      disableUntil: {
        day: this.currentDate.getDate() - 1,
        month: this.currentDate.getMonth() + 1,
        year: this.currentDate.getFullYear()
      },
      dayLabels: {su: 'Dom', mo: 'Lun', tu: 'Mar', we: 'Mer', th: 'Gio', fr: 'Ven', sa: 'Sab'},
      monthLabels: {
        1: 'Gen',
        2: 'Feb',
        3: 'Mar',
        4: 'Apr',
        5: 'Mag',
        6: 'Giu',
        7: 'Lug',
        8: 'Ago',
        9: 'Set',
        10: 'Ott',
        11: 'Nov',
        12: 'Dic'
      },

      showSelectDateText: true,
      selectBeginDateTxt: 'Seleziona data check-in',
      selectEndDateTxt: 'Seleziona data check-out'
    };

    this.type = 'friends';

    this.service.retrieveItineraries(localStorage.getItem('email')).then(result => this.onResultItineraries(result));
    this.service.retrieveCities().then(result => this.cities = result.map(city => city.cityName));
  }

  onResultItineraries(result: string): void {
    this.itineraries = [];
    var currentItineraryID = '';
    var newItinerary = null;

    var places: PlaceItinerary[] = [];

    for (var i = 0; i < result.length; i++) {

      if (currentItineraryID != result[i]['id']) {

        if (newItinerary != null) {
          newItinerary.setPlaces(places);
          this.itineraries.push(newItinerary);
        }
        places = [];
        currentItineraryID = result[i]['id'];
        newItinerary = new Itinerary(result[i]['id'], result[i]['startDay'], result[i]['endDay'], result[i]['userName'], result[i]['surname'], result[i]['cityName']);
        places.push(new PlaceItinerary(result[i]['placeID'], result[i]['namePlace'], result[i]['dayRoute'], result[i]['image'],
          result[i]['address'], result[i]['longDescription'], result[i]['url'], result[i]['linkType']));
      } else {
        places.push(new PlaceItinerary(result[i]['placeID'], result[i]['namePlace'], result[i]['dayRoute'], result[i]['image'],
          result[i]['address'], result[i]['longDescription'], result[i]['url'], result[i]['linkType']));
      }
    }
    if (newItinerary != null) {
      newItinerary.setPlaces(places);
      this.itineraries.push(newItinerary);
    }
  }

  onDateRangeChanged(event: IMyDateRangeModel) {
    this.checkInDateMillis = event.beginEpoc;
    this.checkOutDateMillis = event.endEpoc;
  }

  onInputFieldChanged(event: IMyInputFieldChanged) {
    if (!event.valid) {
      this.checkOutDateMillis = null;
      this.checkInDateMillis = null;
    }
  }

  onLogout(): void {
    localStorage.removeItem("email");
    localStorage.removeItem("name");
    localStorage.removeItem("surname");
    this.user = null;
    this.router.navigate(['/index']);
  }

  onChangeType(type: string) {
    this.type = type;
  }

  onSearch(): void {
    if (this.selectedCity != '' && this.checkInDateMillis != null && this.checkOutDateMillis != null) {
      this.router.navigate(['/home', this.selectedCity, this.checkInDateMillis.toString(), this.checkOutDateMillis.toString(), this.type]);
    } else {
      var alertError = document.getElementById('alert-error-search') as HTMLElement;
      alertError.style.display = 'block';
    }
  }

  openDetails(placeName: string, description: string, image: string, link: string, linkType: string, address: string): void {
    var button = document.getElementById('button-details-modal') as HTMLElement;
    button.click();

    var detailsPlaceName = document.getElementById('detail-place-name') as HTMLElement;
    var detailPlaceImage = document.getElementById('detail-place-image') as HTMLElement;
    var detailPlaceLink = document.getElementById('detail-place-link') as HTMLElement;
    var detailPlaceDescription = document.getElementById('detail-place-description') as HTMLElement;
    var detailPlaceAddress = document.getElementById('detail-place-address') as HTMLElement;

    detailsPlaceName.innerText = placeName;

    detailPlaceLink.setAttribute('href', link);
    detailPlaceLink.innerText = linkType;

    detailPlaceImage.setAttribute('src', image);

    detailPlaceDescription.innerText = description;

    detailPlaceAddress.innerText = address;
  }
}
