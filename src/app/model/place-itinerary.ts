export class PlaceItinerary {

  placeID: string;
  namePlace: string;
  dayRoute: string;
  image: string;

  address: string;
  longDescription: string;
  link: string;
  linkType: string;

  constructor(placeID: string, namePlace: string, dayRoute: string, image: string, address: string, longDescription: string,
                link: string, linkType: string){
    this.placeID = placeID;
    this.namePlace = namePlace;
    this.dayRoute = dayRoute;
    this.image = image;

    this.address = address;
    this.longDescription = longDescription;
    this.link = link;
    this.linkType = linkType;
  }

}
