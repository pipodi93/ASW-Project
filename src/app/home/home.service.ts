import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import "rxjs/add/operator/toPromise";
import {Place} from "../model/place";
import {City} from "../model/city";

@Injectable()
export class HomeService {

  private urlSearch: string = 'http://ubook.store/svapo/search.php';
  private urlSave: string = 'http://ubook.store/svapo/save.php';
  private urlShare: string = 'http://ubook.store/svapo/share.php';
  private urlLogin: string = 'http://ubook.store/svapo/login.php';
  private urlCities: string = 'http://ubook.store/svapo/cities.php';
  private urlRegister: string = 'http://ubook.store/svapo/registerPost.php';

  constructor(private http: Http) { }

  search(city: string, checkin: number, checkout: number, type: string): Promise<Place[]>{
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {city: city, checkin:checkin, checkout: checkout, type: type};

    return this.http.post(this.urlSearch, JSON.stringify(params), headers)
      .toPromise().then(result => result.json() as Place[]);
  }

  save(params: string) : Promise<string>{
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.urlSave, params, headers)
      .toPromise().then(result => result.json());
  }

  login(email:string, password:string): Promise<string>{
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {email: email, password: password};

    return this.http.post(this.urlLogin, JSON.stringify(params), headers)
      .toPromise().then(result => result.json());
  }

  share(params: string): Promise<string> {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    return this.http.post(this.urlShare, params, headers)
      .toPromise().then(result => result.json());
  }

  retrieveCities(): Promise<City[]> {
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {};

    return this.http.post(this.urlCities, JSON.stringify(params), headers)
      .toPromise().then(result => result.json() as City[]);
  }

  register(email:string, password:string, name:string, surname:string): Promise<string>{
    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin','*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers');
    headers.append('Content-Type', 'application/json');

    var params = {email: email, password: password, name: name, surname:surname};

    return this.http.post(this.urlRegister, JSON.stringify(params), headers)
      .toPromise().then(result => result.json());
  }

}
