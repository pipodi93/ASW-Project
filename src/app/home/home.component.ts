import {Component, OnInit} from "@angular/core";
import {User} from "../model/user";
import {ActivatedRoute, Router} from "@angular/router";
import {HomeService} from "./home.service";
import {Place} from "../model/place";
import {Route} from "../model/route";
import {Calendar} from "../model/calendar";
import {PlaceToSave} from "../model/place-to-save";
import {GroupsService} from "../groups/groups.service";
import {Group} from "../model/group";
import {IMyDateRangeModel, IMyDrpOptions, IMyInputFieldChanged} from "mydaterangepicker";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ HomeService, GroupsService ]
})

export class HomeComponent implements OnInit {

  private user: User;
  private type: string;
  private currentType: string;

  private currentDate: Date;
  private checkInDateMillis: number = null;
  private checkOutDateMillis: number = null;
  private selectedCity: string = '';
  private cities: string[];

  private myDateRangePickerOptions: IMyDrpOptions;

  private calendar: Calendar = new Calendar();

  private routes: Route[] = [];
  private suggestedPlaces: Place[] = [];
  private orderedPlaces: Place[] = [];

  private firstDay: number;
  private lastDay: number;

  private groups: Group[] = [];

  constructor(private router: Router, private activatedRoute : ActivatedRoute, private service: HomeService, private groupService: GroupsService) { }

  ngOnInit() {
    if(localStorage.getItem('email') != null) {
      this.user = new User(localStorage.getItem('email'), localStorage.getItem('name'), localStorage.getItem('surname'));
      this.groupService.retrieveGroups(localStorage.getItem('email')).then(result => this.groups = result);
    } else {
      this.user = null;
    }
    this.currentDate = new Date();
    this.myDateRangePickerOptions = {
      dateFormat: 'dd/mm/yyyy',
      disableUntil: {
        day: this.currentDate.getDate() - 1,
        month: this.currentDate.getMonth() + 1,
        year: this.currentDate.getFullYear()
      },
      dayLabels: {su: 'Dom', mo: 'Lun', tu: 'Mar', we: 'Mer', th: 'Gio', fr: 'Ven', sa: 'Sab'},
      monthLabels: {
        1: 'Gen',
        2: 'Feb',
        3: 'Mar',
        4: 'Apr',
        5: 'Mag',
        6: 'Giu',
        7: 'Lug',
        8: 'Ago',
        9: 'Set',
        10: 'Ott',
        11: 'Nov',
        12: 'Dic'
      },
      showSelectDateText: true,
      selectBeginDateTxt: 'Seleziona data check-in',
      selectEndDateTxt: 'Seleziona data check-out'
    };

    this.type = 'friends';

    this.activatedRoute.params.subscribe(params => {
      let city = params['city'];
      let checkin = Number(params['checkin']) * 1000;
      let checkout = Number(params['checkout']) * 1000;
      let type = params['type'];

      this.firstDay = checkin;
      this.lastDay = checkout;

      this.onSearch(city, checkin, checkout, type);
      // TODO Controllo che siano tutti settati, perchè se entro a mano nella pagina home deve
      // TODO comparire un messaggio esempio EFETTUA UNA RICERCA
    });
    this.service.retrieveCities().then(result => this.cities = result.map(city => city.cityName));
  }

  onSearch(city: string, checkin: number, checkout: number, type: string): void {
    this.service.search(city, checkin, checkout, type).then(result => this.onSearchResult(result, checkin, checkout, type));
  }

  onSave(): void {
    var places : PlaceToSave[] = [];
    for (let route of this.routes){
      for (let place of route.places){
        places.push(new PlaceToSave(route.day, place.placeID));
      }
    }

    var checkinDate = new Date(this.firstDay);
    var chekoutDate = new Date(this.lastDay);
    var firstDay = checkinDate.getDate() + " " + this.calendar.getMonth(checkinDate.getMonth()) + " " + checkinDate.getFullYear();
    var lastDay = chekoutDate.getDate() + " " + this.calendar.getMonth(chekoutDate.getMonth()) + " " + chekoutDate.getFullYear();

    var params = {startDay: firstDay, endDay: lastDay, email: localStorage.getItem('email'), places: places, type: this.currentType};
    this.service.save(JSON.stringify(params)).then(result => this.onSaveResult(result));
  }

  onSaveResult(result: string): void {
    var buttonShowDialog = document.getElementById('button-show-save-dialog') as HTMLElement;
    var modalMessage = document.getElementById('modal-message') as HTMLElement;
    if (result['success']){
      modalMessage.innerText = "Itinerario salvato correttamente.";
      buttonShowDialog.click();
    }else {
      modalMessage.innerText = "Si è verificato un errore durante il salvataggio.";
      buttonShowDialog.click();
    }
  }

  onLogin(): void {
    var email = document.getElementById('loginEmail') as HTMLInputElement;
    var password = document.getElementById('loginPassword') as HTMLInputElement;
    if(email.value != '' && password.value != ''){
      this.service.login(email.value, password.value).then(result => this.onLoginResult(result));
    } else {
      var alertError = document.getElementById('alert-error-login') as HTMLElement;
      var message = document.getElementById('error-message-login') as HTMLElement;
      message.innerText = 'Tutti i campi sono obbligatori';
      alertError.style.display = 'block';
    }
  }

  onLoginResult(result: string): void {
    console.log(result);
    if(result['success']) {
      this.user = result['user'] as User;
      localStorage.setItem("email", this.user.email);
      localStorage.setItem("name", this.user.userName);
      localStorage.setItem("surname", this.user.surname);
      var closeButton = document.getElementById('close-button-login');
      closeButton.click();
    } else {
      var alertErrorLogin = document.getElementById('alert-error-login') as HTMLElement;
      var alertMessageLogin = document.getElementById('error-message-login') as HTMLElement;
      alertErrorLogin.style.display = 'block';
      alertMessageLogin.innerText = 'E-Mail o Password errati';
    }
  }

  onLogout():void{
    localStorage.removeItem("email");
    localStorage.removeItem("name");
    localStorage.removeItem("surname");
    this.user = null;
  }

  search(){
    if (this.selectedCity != '' && this.checkInDateMillis != null && this.checkOutDateMillis != null) {
      var alertError = document.getElementById('alert-error-search') as HTMLElement;
      alertError.style.display = 'none';
      this.routes = [];
      this.suggestedPlaces = [];
      this.onSearch(this.selectedCity, this.checkInDateMillis * 1000, this.checkOutDateMillis * 1000, this.type);
    } else {
      var alertError = document.getElementById('alert-error-search') as HTMLElement;
      var message = document.getElementById('error-message') as HTMLElement;
      message.innerText = 'Tutti i campi sono obbligatori';
      alertError.style.display = 'block';
    }
  }

  onSearchResult(places: Place[], checkin: number, checkout: number, type: string): void {
    this.currentType = type;
    var oneDay = 24*60*60*1000;
    var numDays = this.calculateDays(checkin, checkout) + 1;
    var numPlacesPerDay: number = Math.round(places.length / numDays);
    var maxPlacesPerDay: number;
    if(numPlacesPerDay > 3){
      maxPlacesPerDay = 3;
    } else {
      maxPlacesPerDay = numPlacesPerDay;
    }

    var totalPlacesToShow : number = maxPlacesPerDay * numDays;
    var placesToShow: Place[] = [];
    for(var i=0; i<totalPlacesToShow && i<places.length; i++){
      placesToShow.push(places[i]);
    }

    var orderedPlaces: Place[] = this.orderList(placesToShow);
    this.orderedPlaces = orderedPlaces;

    for(var j=0; j<numDays; j++){
      var date = new Date(checkin + oneDay * j);
      var placesPerDay: Place[] = [];

      for(var k=maxPlacesPerDay*j, times=0; times<maxPlacesPerDay && k<orderedPlaces.length; k++, times++){
        placesPerDay.push(orderedPlaces[k]);
      }
      placesPerDay[placesPerDay.length - 1].distanceTo = null;
      var dateString = this.calendar.getDay(date.getDay()) + " " + date.getDate() + " " + this.calendar.getMonth(date.getMonth());
      var route = new Route(dateString, this.calendar.getDay(date.getDay()) + ' ' + date.getDate() + ' ' +
        this.calendar.getMonth(date.getMonth()), placesPerDay);
      this.routes.push(route);
    }

    for(var l=totalPlacesToShow; l<places.length; l++){
      this.suggestedPlaces.push(places[l]);
    }

  }

  onShare():void{
    var groupSelection = document.getElementById('sel1') as HTMLSelectElement;
    var errorSelection = document.getElementById('alert-error-share') as HTMLElement;
    var successSelection = document.getElementById('alert-success-share') as HTMLElement;
    var selectedIndex = groupSelection.selectedIndex - 1;
    if (selectedIndex == -1){
      errorSelection.style.display = 'block';
    }else {
      errorSelection.style.display = 'none';

      var places : PlaceToSave[] = [];
      for (let route of this.routes){
        for (let place of route.places){
          places.push(new PlaceToSave(route.day, place.placeID));
        }
      }

      var checkinDate = new Date(this.firstDay);
      var chekoutDate = new Date(this.lastDay);
      var firstDay = checkinDate.getDate() + " " + this.calendar.getMonth(checkinDate.getMonth()) + " " + checkinDate.getFullYear();
      var lastDay = chekoutDate.getDate() + " " + this.calendar.getMonth(chekoutDate.getMonth()) + " " + chekoutDate.getFullYear();

      var params = {groupID: this.groups[selectedIndex].id, startDay: firstDay, endDay: lastDay, email: localStorage.getItem('email'), places: places, type: this.currentType};
      this.service.share(JSON.stringify(params)).then(result => this.onShareResult(result));
    }
  }

  onShareResult(result: string): void {
    var successSelection = document.getElementById('alert-success-share') as HTMLElement;
    if(result['success']){
      successSelection.style.display = 'block';
    } else {
      // TODO DIOBO VIZ
    }
  }

  // Prende in ingresso la lista dei posti che devono essere mostrati
  // nella colonna centrale e li ordina per distanza da uno all'altro
  orderList(places: Place[]): Place[]{
    var returnList: Place[] = [];
    if(places.length == 0){
      returnList;
    }

    returnList.push(places[0]);
    var lastPlace: Place = places[0];
    places.splice(0, 1);

    while(places.length != 0){
      var minDistanceIndex: number = 0;
      var minDistance: number = 0;
      for(var i=0; i<places.length; i++){
        var d = this.calculateDistance(lastPlace.lat, lastPlace.lng, places[i].lat, places[i].lng);
        if(d < minDistance || minDistance == 0){
          minDistanceIndex = i;
          minDistance = d;
        }
      }

      returnList.push(places[minDistanceIndex]);
      lastPlace.distanceTo = Math.trunc(minDistance);
      lastPlace.distanceInMinutes = Math.trunc(minDistance/60);
      lastPlace = places[minDistanceIndex];
      places.splice(minDistanceIndex, 1);
    }

    return returnList;
  }

  addPlace(index: number): void{
    var indexMinPlacesPerDay: number = 0;
    var minPlacesPerDay: number = -1;
    for(var i=0; i<this.routes.length; i++){
      if(this.routes[i].places.length < minPlacesPerDay || minPlacesPerDay == -1){
        minPlacesPerDay = this.routes[i].places.length;
        indexMinPlacesPerDay = i;
      }
    }
    this.routes[indexMinPlacesPerDay].places.push(this.suggestedPlaces[index]);
    this.suggestedPlaces.splice(index, 1);
    this.refreshTrip();
  }

  deletePlace(indexRoute: number, indexPlace: number): void {
    var p: Place = this.routes[indexRoute].places[indexPlace];
    this.suggestedPlaces.push(p);
    this.routes[indexRoute].places.splice(indexPlace,1);

    this.refreshTrip();
  }

  refreshTrip(){
    for(let elem of this.routes){
      if(elem.places.length > 0){
        elem.places = this.orderList(elem.places);
      }
      if(elem.places.length > 0){
        elem.places[elem.places.length - 1].distanceTo = null;
      }
    }
  }

  calculateDays(checkinMills : number, checkoutMills: number): number{
    var oneDay = 24*60*60*1000;
    return Math.round(Math.abs((checkinMills - checkoutMills)/(oneDay)));
  }

  calculateDistance(lat1: number, lon1: number, lat2: number, lon2: number): number {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2-lon1);
    var a =
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon/2) * Math.sin(dLon/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;

    return d * 1000;
  }

  deg2rad(deg) {
    return deg * (Math.PI/180)
  }

  onChangeType(type: string){
    this.type = type;
  }

  clearModals(){
    var alertError = document.getElementById('alert-error') as HTMLElement;
    var alertSuccess = document.getElementById('alert-success') as HTMLElement;
    var alertErrorLogin = document.getElementById('alert-error-login') as HTMLElement;
    alertSuccess.style.display = 'none';
    alertError.style.display = 'none';
    alertErrorLogin.style.display = 'none';

    // Resetto la form di login
    var email = document.getElementById('loginEmail') as HTMLInputElement;
    var password = document.getElementById('loginPassword') as HTMLInputElement;
    email.value = '';
    password.value = '';

    // Resetto la form di registrazione
    var email = document.getElementById('registerEmail') as HTMLInputElement;
    var password = document.getElementById('registerPassword') as HTMLInputElement;
    var name = document.getElementById('registerName') as HTMLInputElement;
    var surname = document.getElementById('registerSurname') as HTMLInputElement;
    email.value = '';
    password.value = '';
    name.value = '';
    surname.value = '';

    //Resetto la form di condivisione
    var alertSuccessShare = document.getElementById('alert-success-share') as HTMLElement;
    var alertErrorShare = document.getElementById('alert-error-share') as HTMLElement;
    alertErrorShare.style.display = 'none';
    alertSuccessShare.style.display = 'none';

  }

  onDateRangeChanged(event: IMyDateRangeModel) {
    this.checkInDateMillis = event.beginEpoc;
    this.checkOutDateMillis = event.endEpoc;
  }

  onInputFieldChanged(event: IMyInputFieldChanged) {
    if (!event.valid) {
      this.checkOutDateMillis = null;
      this.checkInDateMillis = null;
    }
  }

  onRegister(): void{
    var email = document.getElementById('registerEmail') as HTMLInputElement;
    var password = document.getElementById('registerPassword') as HTMLInputElement;
    var name = document.getElementById('registerName') as HTMLInputElement;
    var surname = document.getElementById('registerSurname') as HTMLInputElement;

    var patternEmail = new RegExp('^\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}$');

    if(email.value != '' && password.value != '' && name.value != '' && surname.value != '' && patternEmail.test(email.value)){
      this.service.register(email.value, password.value, name.value, surname.value).then(result => this.onRegisterResult(result));
    } else {
      var alertError = document.getElementById('alert-error') as HTMLElement;
      var alertSuccess = document.getElementById('alert-success') as HTMLElement;
      var message = document.getElementById('error-message') as HTMLElement;
      message.innerText = 'Riempire tutti i campi correttamente';
      alertError.style.display = 'block';
      alertSuccess.style.display = 'none';
    }
  }

  onRegisterResult(result: string): void {
    console.log(result);
    if(result['success']){
      var alertSuccess = document.getElementById('alert-success') as HTMLElement;
      var alertError = document.getElementById('alert-error') as HTMLElement;
      alertSuccess.style.display = 'block';
      alertError.style.display = 'none';
    } else {
      var alertError = document.getElementById('alert-error') as HTMLElement;
      var alertSuccess = document.getElementById('alert-success') as HTMLElement;
      var message = document.getElementById('error-message') as HTMLElement;
      message.innerText = 'E-mail già in uso da un altro utente';
      alertError.style.display = 'block';
      alertSuccess.style.display = 'none';
    }
  }

  openDetails(placeName: string, description: string, image: string, link: string, linkType: string, address: string): void {
    var button = document.getElementById('button-details-modal') as HTMLElement;
    button.click();

    var detailsPlaceName = document.getElementById('detail-place-name') as HTMLElement;
    var detailPlaceImage = document.getElementById('detail-place-image') as HTMLElement;
    var detailPlaceLink = document.getElementById('detail-place-link') as HTMLElement;
    var detailPlaceDescription = document.getElementById('detail-place-description') as HTMLElement;
    var detailPlaceAddress = document.getElementById('detail-place-address') as HTMLElement;

    detailsPlaceName.innerText = placeName;

    detailPlaceLink.setAttribute('href', link);
    detailPlaceLink.innerText = linkType;

    detailPlaceImage.setAttribute('src', image);

    detailPlaceDescription.innerText = description;

    detailPlaceAddress.innerText = address;
  }

}
