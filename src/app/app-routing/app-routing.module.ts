import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";

import {HomeComponent} from "../home/home.component";
import {IndexComponent} from "../index/index.component";
import {GroupsComponent} from "../groups/groups.component";
import {ProfileComponent} from "../profile/profile.component";
import {SuggestComponent} from "../suggest/suggest.component";

const routes: Routes = [
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index',  component: IndexComponent },
  { path: 'home',  component: HomeComponent },
  { path: 'home/:city/:checkin/:checkout/:type', component: HomeComponent },
  { path: 'groups', component: GroupsComponent },
  {path: 'profile', component: ProfileComponent},
  {path: 'suggest', component: SuggestComponent}
];

@NgModule({
  /*imports: [
    CommonModule
  ],
  declarations: []*/
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }
