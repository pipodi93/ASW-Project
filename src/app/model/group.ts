export class Group {

  id: string;
  groupName: string;
  admin: string;
  memberNum: number;

  constructor(id: string, groupName: string, admin: string, memberNum: number){
    this.id = id;
    this.groupName = groupName;
    this.admin = admin;
    this.memberNum = memberNum;
  }

}
