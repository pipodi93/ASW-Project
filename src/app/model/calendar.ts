export class Calendar {


  weekday = new Array(7);
  months = new Array(12);

  constructor(){
    this.weekday[0] =  "Domenica";
    this.weekday[1] = "Lunedì";
    this.weekday[2] = "Martedì";
    this.weekday[3] = "Mercoledì";
    this.weekday[4] = "Giovedì";
    this.weekday[5] = "Venerdì";
    this.weekday[6] = "Sabato";

    this.months[0] = "Gennaio";
    this.months[1] = "Febbraio";
    this.months[2] = "Marzo";
    this.months[3] = "Aprile";
    this.months[4] = "Maggio";
    this.months[5] = "Giugno";
    this.months[6] = "Luglio";
    this.months[7] = "Agosto";
    this.months[8] = "Settembre";
    this.months[9] = "Ottobre";
    this.months[10] = "Novembre";
    this.months[11] = "Dicembre";
  }

  getDay(day:number) : string{
    return this.weekday[day];
  }

  getMonth(month:number) : string{
    return this.months[month];
  }

}
